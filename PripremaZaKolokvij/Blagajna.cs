﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PripremaZaKolokvij {
    class Blagajna {

        private List<IRacun> lista;

        public Blagajna() {
            lista = new List<IRacun>();
        }

        public void DodajRacun(IRacun racun) {
            lista.Add(racun);
        }

        public void PrikaziRacune() {
            foreach (var racun in lista) {
                Console.WriteLine(racun.DohvatiIznos());
            }

        }

        
    }
}


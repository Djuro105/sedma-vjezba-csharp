﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PripremaZaKolokvij {
    class Racun<T> : IRacun {
        DateTime datumIzdavanja;
        T iznosRacuna;

        public Racun(T iznosRacuna) {
            DatumIzdavanja = DateTime.Now;
            IznosRacuna = iznosRacuna;
        }

        public decimal DohvatiIznos() {
            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime DohvatiDatumIzdavanja() {
            return DatumIzdavanja;
        }

        public DateTime DatumIzdavanja { get; set; }

        public T IznosRacuna {
            get {
                return iznosRacuna;
            }
            set {
                if(Convert.ToInt32(value) < 10) {
                    throw new IznimkaBlagajne("Premali iznos");
                   
                }
                iznosRacuna = value; 
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PripremaZaKolokvij {
    class Program {
        static void Main(string[] args) {

            Blagajna blag = new Blagajna();

            Racun<int> r1 = new Racun<int>(20);
            Racun<string> r2 = new Racun<string>("20");

            blag.DodajRacun(r1);
            blag.DodajRacun(r2);

            blag.PrikaziRacune();
        }
    }
}
